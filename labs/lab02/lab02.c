#define WOKWI             // Uncomment if running on Wokwi RP2040 emulator.

#include <stdio.h>
#include <stdlib.h>
//#include "pico/stdlib.h"
#include "pico/float.h"
#include "pico/double.h"

#define MAX_ITERATIONS 100000
#define PI_GIVEN 3.14159265359
 //the code of the wallis equation
float SinglePrecision(int iterations)
{
float product = 1.0;
for (int i = 1; i <= iterations; i++)
{
product = product * ((float) (2 * i) / (2 * i - 1)) * ((float) (2 * i) / (2 * i + 1));
}
product =  2 * product;
return product;
}

double DoublePrecision(int iterations)
{
double product = 1.0;
for (int i = 1; i <= iterations; i++)
{
product = product * ((double) (2 * i) / (2 * i - 1)) * ((double) (2 * i) / (2 * i + 1));
}
product = 2 * product;
return product;
}



int main() {

    // Initialise the IO as we will be using the UART
    // Only required for hardware and not needed for Wokwi
//stdio_init_all();


// Calculate PI using single precision
float Single = SinglePrecision(MAX_ITERATIONS);
printf("Calculated PI using single precision: %.10f\n", Single);
float SingleError = fabs(Single - PI_GIVEN);
printf("Approximation error using single precision: %.10f\n\n", SingleError);

// Calculate PI using double precision
double Double = DoublePrecision(MAX_ITERATIONS);
printf("Calculated PI using double precision: %.20lf\n", Double);
double DoubleError = fabs(Double - PI_GIVEN);
printf("Approximation error using double precision: %.20lf\n\n", DoubleError);

// Return 0 to indicate success
return 0;
}



// int main() {

//     // Initialise the IO as we will be using the UART
//     // Only required for hardware and not needed for Wokwi
// //stdio_init_all();


// // Calculate PI using single precision floating-point representation
// float Single = SinglePrecision(MAX_ITERATIONS);
// printf("Calculated PI using single precision: %.10f\n", Single);
// float SingleError = fabs(Single - PI_GIVEN);
// printf("Approximation error using single precision: %.10f\n\n", SingleError);

// // Calculate PI using double precision floating-point representation
// double Double = DoublePrecision(MAX_ITERATIONS);
// printf("Calculated PI using double precision: %.20lf\n", Double);
// double DoubleError = fabs(Double - PI_GIVEN);
// printf("Approximation error using double precision: %.20lf\n\n", DoubleError);

// // Return 0 to indicate success
// return 0;
// }